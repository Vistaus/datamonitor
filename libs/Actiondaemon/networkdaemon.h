#ifndef NETWORKDAEMON_H
#define NETWORKDAEMON_H

#include <QNetworkConfigurationManager>

class Networkdaemon {

public:

    QList<QVariant> recBytes();
    quint64 recBytesFromCMD();
    void shootNotification(const QVariant connType, const QVariant totBytes);
    void resetNotification(const QVariant connType);

};


#endif
