#include <stdint.h>
#include <QNetworkSession>
#include <QNetworkConfigurationManager>
#include <fstream>
#include <iostream>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QProcess>

#include "networkdaemon.h"

using namespace std;

QList<QVariant> Networkdaemon::recBytes() {
  quint64 bytes;
  QList<QVariant> connBytes;
  QNetworkConfigurationManager manager;
  manager.updateConfigurations();
  int count=0;
  QList<QNetworkConfiguration> activeConfigs = manager.allConfigurations(QNetworkConfiguration::Active);
  const bool Datastat = (manager.capabilities() & QNetworkConfigurationManager::DataStatistics);
  if (activeConfigs.count()>0) {
    if (Datastat) {
      qDebug() << "Platform is capable of data statistics for available connections.";
    }  else {
      qDebug() << "Platform is not capable of data statistics for available connections.";
    }
  }
int j = 0;
for (int i=0; i < activeConfigs.count(); ++i) {
      qDebug() << activeConfigs.value(i).name();
      qDebug() << activeConfigs.value(i).bearerTypeName();
      qDebug() << activeConfigs.value(i).identifier();
      if (activeConfigs.value(i).bearerTypeFamily()==3 || activeConfigs.value(i).bearerTypeFamily()==11 || activeConfigs.value(i).bearerTypeFamily()==12) {
        connBytes << "SIM";
      } else {
        if (activeConfigs.value(i).bearerTypeFamily()==2) {
          connBytes << "WIFI";
        }
      }
      QNetworkConfiguration cfg = manager.configurationFromIdentifier(activeConfigs.value(i).identifier());
      QNetworkSession *session = new QNetworkSession(cfg);
      if (session->isOpen()==false) {
        session->open();
        session->waitForOpened(1000);
      } else {
        qDebug() << "Session is open!";
      }
      if (session->isOpen()) {
        qDebug() << "Session is open!";
        if (session->state()==session->Connected) {
          qDebug() << "Session connected!";
        } else {
            if (session->state()==session->NotAvailable) {
              qDebug() << "Session not available.";
            } else {
              if (session->state()==session->Connecting) {
                qDebug() << "Session connecting...";
              } else {
                if (session->state()==session->Invalid) {
                  qDebug() << "Session not valid.";
                } else {
                  if (session->state()==session->Closing) {
                    qDebug() << "Session closing...";
                  } else {
                    if (session->state()==session->Disconnected) {
                      qDebug() << "Session disconnected.";
                    } else {
                      if (session->state()==session->Roaming) {
                        qDebug() << "Session is roaming...";
                      } else {
                        qDebug() << "Session state not identified...";
                      }
                    }
                  }
                }
              }
          }
        }
      } else {
        qDebug() << "Session is still closed.";
      }
//      if (connBytes[j]=="SIM" || connBytes[j]=="WIFI") {
      if (connBytes[j]=="WIFI") {
        bytes = session->bytesReceived();
        connBytes << bytes;
        j= j+2;
      } else if (connBytes[j]=="SIM") {
        bytes = Networkdaemon::recBytesFromCMD();
        connBytes << bytes;
        j= j+2;
      }
}
  return connBytes;
}

quint64 Networkdaemon::recBytesFromCMD() {
  bool isUp=false;
  bool isRunning=false;
  QVariant deviceName;
  QString deviceNameStr;
  string receivedbytesStr;
  QNetworkInterface interfacesAll;
  QList<QNetworkInterface> interfacesList = interfacesAll.allInterfaces();
  if (interfacesList.count()>0) {
    int i=1;
    bool device_found = false;
    while (i <= interfacesList.count() & device_found == false) {
      isUp = (interfacesList.value(i).flags() & QNetworkInterface::IsUp);
      if (isUp) {
        isRunning = (interfacesList.value(i).flags() & QNetworkInterface::IsRunning);
        if (isRunning) {
          const bool isPointToPoint = (interfacesList.value(i).flags() & QNetworkInterface::IsPointToPoint);
          if (isPointToPoint) {
             qDebug() << "Interface is a Point to Point type.";
          }
          const bool canBroadcast = (interfacesList.value(i).flags() & QNetworkInterface::CanBroadcast);
          if (canBroadcast) {
            qDebug() << "Interface is able to broadcast.";
          }
          const bool isloopBack = (interfacesList.value(i).flags() & QNetworkInterface::IsLoopBack);
          if (isloopBack) {
            qDebug() << "Interface is a loopback type.";
          }
          const bool canMulticast = (interfacesList.value(i).flags() & QNetworkInterface::CanMulticast);
          if (canMulticast) {
            qDebug() << "Interface is able to multicast.";
          }
//          if (isPointToPoint) {
            device_found = true;
            deviceName = interfacesList.value(i).name();
            deviceNameStr = deviceName.toString();
            qDebug() << "Interface for running SIM is:" << deviceNameStr;
//          }
        }
      }
      i = i+1;
    }
  } else {
    qDebug() << "No interfaces found for the SIM connection, therefore received data storage is not available.";
  }
  if (isUp) {
      if (!isRunning) {
         qDebug() << "One interface for the SIM connection is up but not running, therefore received data monitoring and storage is not available.";
      }
  } else {
    qDebug() << "None of the interfaces for the SIM connection is up, therefore received data monitoring and storage is not available.";
  }
  if (deviceName=="") {
    return 0;
  } else {
    fstream rxFile;
    string address = "/sys/class/net/";
    string deviceName_text = deviceNameStr.toUtf8().constData();
    address += deviceName_text + "/statistics/rx_bytes";
    rxFile.open(address.c_str(),ios::in);
    rxFile>>receivedbytesStr;
    QString receivedBytes = QString::fromStdString(receivedbytesStr);
    quint64 receivedBytesBack = receivedBytes.toDouble();
    rxFile.close();
    return receivedBytesBack;
  }
}

void Networkdaemon::shootNotification(const QVariant connType, const QVariant totBytes) {
  string tokenID;
  bool enableNotify;
  QString message;
  fstream tokenFile;
  fstream notifyFile;
  QVariant tokenDest;
  QVariant sizeDB = 0;
  QVariant dataBytes = 0;
  QVariant silenFlag = 0;
  QVariant notifFlag = 0;
  QList<QVariant> numThresholds;
  QList<QVariant> numThresholds_new;
  QQmlEngine engine;
  engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
  QQmlComponent component(&engine,
      QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/ThresholdsHandler.qml")));
  if (component.isError()) {
      qWarning() << component.errors();
  }
  QObject *object = component.create();
  QMetaObject::invokeMethod(object, "getSize",
                    Q_RETURN_ARG(QVariant, sizeDB),
                    Q_ARG(QVariant, connType));
  for (int i=0; i < sizeDB.toInt(); ++i) {
      QMetaObject::invokeMethod(object, "getSilFlag",
                        Q_RETURN_ARG(QVariant, silenFlag),
                        Q_ARG(QVariant, i),
                        Q_ARG(QVariant, connType));
      if (silenFlag==0) {
          QMetaObject::invokeMethod(object, "getNotifFlag",
                            Q_RETURN_ARG(QVariant, notifFlag),
                            Q_ARG(QVariant, i),
                            Q_ARG(QVariant, connType));
          if (notifFlag==0) {
            QMetaObject::invokeMethod(object, "getValue",
                              Q_RETURN_ARG(QVariant, dataBytes),
                              Q_ARG(QVariant, i),
                              Q_ARG(QVariant, connType));
            if (dataBytes!=0 & dataBytes<=totBytes) {
                numThresholds << dataBytes;
                QMetaObject::invokeMethod(object, "updateNotifFlag",
                                  Q_ARG(QVariant, 1),
                                  Q_ARG(QVariant, dataBytes),
                                  Q_ARG(QVariant, connType));
            }
          }
      }
  }
  delete object;
  QVariant min_value = 0;
  QVariant max_value = 0;
  bool inside = false;
  int k = 0;
  if (numThresholds.size()>0) {
    if (numThresholds.size()==1) {
        numThresholds_new=numThresholds;
    } else {
        numThresholds_new<<k;
    }
  }
  if (numThresholds.size()>1) {   // Algorithm for ordering array from the smallest value to the biggest
        for (int j=0; j < numThresholds.size(); ++j) {
              if (numThresholds[j]>numThresholds_new[j]) {
                  min_value = numThresholds[j];
              } else {
                  min_value = 100000000000;
              }
              while (k < numThresholds.size()) {
                      if (numThresholds[k]>numThresholds_new[j]) {
                        if (numThresholds[k]< min_value) {
                          min_value=numThresholds[k];
                        } else {
                          if (numThresholds[k]>max_value) {
                            max_value=numThresholds[k];
                          }
                        }
                      }
                    k=k+1;
              }
              if (j<numThresholds.size()-1) {
                  numThresholds_new[j]=min_value;
                  numThresholds_new<<min_value;
              } else {
                numThresholds_new[j]=max_value;
              }
              k=0;
        }
  }
  for (int i=0; i < numThresholds_new.size(); ++i) {
    QString message = connType.toString() + " threshold set at " + QString::number(numThresholds_new[i].toDouble(), 'f', 0) + " Mbytes exceeded!";
    qDebug() << message;
    string messageSTDString = message.toStdString();
        string command = "/usr/bin/gdbus call --session --dest com.ubuntu.Postal --object-path /com/ubuntu/Postal/datamonitor_2ematteobellei --method com.ubuntu.Postal.Post datamonitor.matteobellei_datamonitor ";
        command = command + "'\"{\\\"message\\\": \\\"foobar\\\", \\\"notification\\\":{\\\"card\\\": {\\\"icon\\\": \\\"notification\\\", \\\"summary\\\": \\\"dataMonitor\\\", \\\"body\\\": " + "\\\"" + messageSTDString + "\\\", \\\"popup\\\": true, \\\"persist\\\": true}, \\\"sound\\\": true, \\\"vibrate\\\": {\\\"pattern\\\": [200, 100], \\\"duration\\\": 200,\\\"repeat\\\": 2 }}}\"'";
        FILE* pipe = popen(command.c_str(), "r");
  }
}

void Networkdaemon::resetNotification(const QVariant connType) {
  QVariant sizeDB = 0;
  QVariant dataBytes = 0;
  QVariant notifFlag = 0;
  QQmlEngine engine;
  engine.setOfflineStoragePath(QStringLiteral("/home/phablet/.local/share/datamonitor.matteobellei"));
  QQmlComponent component(&engine,
      QUrl(QStringLiteral("/opt/click.ubuntu.com/datamonitor.matteobellei/current/qml/ThresholdsHandler.qml")));
  if (component.isError()) {
      qWarning() << component.errors();
  }
  QObject *object = component.create();
  QMetaObject::invokeMethod(object, "getSize",
                    Q_RETURN_ARG(QVariant, sizeDB),
                    Q_ARG(QVariant, connType));
  for (int i=0; i < sizeDB.toInt(); ++i) {
    QMetaObject::invokeMethod(object, "getNotifFlag",
                      Q_RETURN_ARG(QVariant, notifFlag),
                      Q_ARG(QVariant, i),
                      Q_ARG(QVariant, connType));
    if (notifFlag==1) {
      QMetaObject::invokeMethod(object, "getValue",
                        Q_RETURN_ARG(QVariant, dataBytes),
                        Q_ARG(QVariant, i),
                        Q_ARG(QVariant, connType));
      QMetaObject::invokeMethod(object, "updateNotifFlag",
                        Q_ARG(QVariant, 0),
                        Q_ARG(QVariant, dataBytes),
                        Q_ARG(QVariant, connType));
    }
  }
}
