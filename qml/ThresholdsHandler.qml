import QtQuick 2.4
import QtQuick.LocalStorage 2.0
import "StorageThresholds.js" as StorageThresholds
import Ubuntu.PushNotifications 0.1

Item {

  function getSize(connType)
	{
			var numStrings = StorageThresholds.getNumData(connType);
			return numStrings;
	}

	function getValue(index, connType)
	{
			var mbData = StorageThresholds.getValueFromPos(index, connType);
			return mbData;
	}

	function getSilFlag(index, connType)
	{
			var ismute = StorageThresholds.getMuteFromPos(index, connType);
			return ismute;
	}

	function getNotifFlag(index, connType)
	{
			var isNotif = StorageThresholds.getNotifFlagFromPos(index, connType);
			return isNotif;
	}

	function updateNotifFlag(flagNot, value,  connType)
	{
			StorageThresholds.updateNotifFlagValue(flagNot, value, connType);
	}

  function triggerNotification(tokenID, message)
	{
      console.log("💬 Sending push notification ...")
      var req = new XMLHttpRequest();
      req.open("post", "https://push.ubports.com/notify", true);
      req.setRequestHeader("Content-type", "application/json");
      print("Initial state:" + req.readyState)
      req.onreadystatechange = function() {
              print("Changed state:" + req.readyState)
              if ( req.readyState === XMLHttpRequest.DONE ) {
                  console.log("✍ Answer from push service:", req.responseText)
                  var ans = JSON.parse(req.responseText)
                  if ( ans.error ) {
                      console.log(ans.error)
                      if ( ans.message ) {
                          console.log(ans.message)
                      }
                  }
              }
      }
      var approxExpire = new Date ()
      approxExpire.setUTCMinutes(approxExpire.getUTCMinutes()+10)
      print("Final state:" + req.readyState)
      req.send(JSON.stringify({
          "appid" : "datamonitor.matteobellei_datamonitor",
          "expire_on": approxExpire.toISOString(),
//          "token": tokenID,
          "token": "ZGF0YW1vbml0b3IubWF0dGVvYmVsbGVpX2RhdGFtb25pdG9yOjpjU0lYS0hLL2NWdXE4a2h3OC8rOXdaeEVWZHVSOG96NGRiMCtaZz09",
          "data": {
              "notification": {
                  "card": {
                      "icon": "notification",
                      "summary": "Push Notification",
                      "body": message,
                      "popup": true,
                      "persist": true
                  },
                  "vibrate": true,
                  "sound": true
              }
          }
      }))
      var a = 10
      for (var i=1; i <= 5000; i++) {
         if (i==a) {
            print("i= " + i)
            a=a*2
         }
      }
  }

}
