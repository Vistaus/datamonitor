/* QChart.qml ---
 *
 * Author: Julien Wintz
 * Created: Thu Feb 13 20:59:40 2014 (+0100)
 * Version:
 * Last-Updated: jeu. mars  6 12:55:14 2014 (+0100)
 *           By: Julien Wintz
 *     Update #: 69
 */

/* Change Log:
 *
 */

import QtQuick 2.4

import "QChart.js" as Charts

Canvas {

  id: canvas;

// ///////////////////////////////////////////////////////////////

  property   var chart;
  property   var chartData;
  property   int chartType: 0;
  property  bool chartAnimated: true;
  property alias chartAnimationEasing: chartAnimator.easing.type;
  property alias chartAnimationDuration: chartAnimator.duration;
  property   int chartAnimationProgress: 0;
  property   var chartOptions: ({})

  signal finishedPaint()

// /////////////////////////////////////////////////////////////////
// Callbacks
// /////////////////////////////////////////////////////////////////

  onPaint: {
      var ctx = canvas.getContext("2d");
      /* Reset the canvas context to allow resize events to properly redraw
         the surface with an updated window size */
      ctx.reset()

      switch(chartType) {
      case Charts.ChartType.BAR:
          chart = new Charts.Chart(canvas, ctx).Bar(chartData, chartOptions);
          break;
      case Charts.ChartType.DOUGHNUT:
          chart = new Charts.Chart(canvas, ctx).Doughnut(chartData, chartOptions);
          break;
      case Charts.ChartType.LINE:
          chart = new Charts.Chart(canvas, ctx).Line(chartData, chartOptions);
          break;
      case Charts.ChartType.PIE:
          chart = new Charts.Chart(canvas, ctx).Pie(chartData, chartOptions);
          break;
      case Charts.ChartType.POLAR:
          chart = new Charts.Chart(canvas, ctx).PolarArea(chartData, chartOptions);
          break;
      case Charts.ChartType.RADAR:
          chart = new Charts.Chart(canvas, ctx).Radar(chartData, chartOptions);
          break;
      default:
          console.log('Chart type should be specified.');
      }

      chart.init();

//      var height = chart.height();
//      settings.axisY = height;
//      var width = chart.width();
//      settings.axisX = width;
//      var yMax = chart.maxYscale();
//      settings.maxScaleY = yMax;


      if (chartAnimated)
          chartAnimator.start();
      else
          chartAnimationProgress = 100;

      chart.draw(chartAnimationProgress/100);
//      var errX = chart.Xclearance();
//      settings.errorX = errX;
//      var errY = chart.Yclearance();
//      settings.errorY = errY;
      graphXYAxis();
      finishedPaint();
  }

  onHeightChanged: {
    requestPaint();
  }

  onWidthChanged: {
    requestPaint();
  }

  onChartAnimationProgressChanged: {
      requestPaint();
  }

  onChartDataChanged: {
      requestPaint();
  }

// /////////////////////////////////////////////////////////////////
// Functions
// /////////////////////////////////////////////////////////////////

  function repaint() {
      chartAnimationProgress = 0;
      chartAnimator.start();
  }

  function graphXYAxis() {
      var height = canvas.chart.height();
      settings.axisY = height;
      var width = canvas.chart.width();
      settings.axisX = width;
      var yMax = canvas.chart.maxYscale();
      if (settings.dataType=="SIM") {
        settings.maxScaleYSIM = yMax;
      } else {
        if (settings.dataType=="WIFI") {
          settings.maxScaleY = yMax;
        }
      }
      var errX = canvas.chart.Xclearance();
      settings.errorX = errX;
      var errY1 = canvas.chart.Y1clearance();
      settings.errorY1 = errY1;
      var errY2 = canvas.chart.Y2clearance();
      settings.errorY2 = errY2;
  }

// /////////////////////////////////////////////////////////////////
// Internals
// /////////////////////////////////////////////////////////////////

  PropertyAnimation {
             id: chartAnimator;
         target: canvas;
       property: "chartAnimationProgress";
             to: 100;
       duration: 500;
    easing.type: Easing.InOutElastic;
  }
}
