import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.Pickers 1.3
//import Ubuntu.Components.ListItems 1.3 as ListItems

/* Thanks to: https://github.com/jwintz/qchart.js for QML bindings for Charts.js */
import "QChart.js" as Charts

/* replace the 'incomplete' QML API U1db with the low-level QtQuick API */
import QtQuick.LocalStorage 2.0

/* Import our javascript files */
import "ChartUtils.js" as ChartUtils
import "DateUtils.js"  as DateUtils
import "Storage.js" as Storage
import "StorageThresholds.js" as StorageThresholds


/*
  NEW: page: added for chapter about "QML and charts"
  Page that display a monthly received Mbytes chart and his legend
*/
Page {
     id: chartPage
     visible: false

     /* default is today, after is updated when the user chose a date with the TimePicker */
     property double summitArrowOffset: 14.43375673
     property string pageHeaderTitle : "SIM analytic page";
     property string targetDate : Qt.formatDateTime(new Date(), "yyyy-MM-dd");
     property var objects : [];
     property double maxScaleGraph : settings.maxScaleY;
     property double maxScaleGraphStored: 0;
     property string typeData: "MBytes"
     property string colorLabelThresh: "transparent"
     property string graphXLabel: "Days of Month"

     header: PageHeader {
          title: i18n.tr(pageHeaderTitle)

          StyleHints	{
            foregroundColor: fontColor
            backgroundColor: bkgColor
            dividerColor:	UbuntuColors.slate
          }
     }

     Rectangle {
         color: bkgColor
         width: parent.width
         height: parent.height
     }

     Component.onCompleted: {

         graphXLabel = i18n.tr("Days of Month");
         if (settings.dataType == "WIFI") {
            pageHeaderTitle =  i18n.tr("Wi-fi analytic page");
            maxScaleGraphStored = settings.maxScaleYStored;
         } else {
            pageHeaderTitle =  i18n.tr("SIM analytic page");
            maxScaleGraph = settings.maxScaleYSIM;
            maxScaleGraphStored = settings.maxScaleYSIMStored;
         }

         drawGraph(settings.dataType);

        var numStrings = StorageThresholds.getNumData(settings.dataType);
        for (var i=0; i < numStrings; i++) {
            var mbData = StorageThresholds.getValueFromPos(i, settings.dataType);
            var yObj = (maxScaleGraph*(settings.axisY-settings.errorY1) - mbData*(settings.axisY-settings.errorY1-settings.errorY2))/maxScaleGraph-25;
            createObj(yObj, mbData);
            initOrdDataModel(mbData);
        }
        if (numStrings==0) {
               colorLabelThresh=fontColor;
        }
     }

     ListModel {
         id: dataModel
     }

     Column{
        id: chartPageMainColumn
        spacing: units.gu(2)
        anchors.fill: parent

        /* transparent placeholder: to place the content under the header */
        Rectangle {
            color: bkgColor
            width: parent.width
            height: units.gu(6)
        }

        Row{
            id: monthSelectorRow
            anchors.horizontalCenter: parent.horizontalCenter
            spacing:units.gu(2)

            /* Create a PopOver containing a DatePicker */
            Component {
                id: popoverTargetMonthPicker
                Popover {
                    id: popoverDatePicker

                    DatePicker {
                        id: timePicker
                        mode: "Months|Years"
                        minimum: {
                            var time = new Date()
                            time.setFullYear('2000')
                            return time
                        }
                        /* when Datepicker is closed, is updated the date shown in the button */
                        Component.onDestruction: {
                            targetMonthSelectorButton.text = Qt.formatDateTime(timePicker.date, "MMMM yyyy");
                            chartPage.targetDate = Qt.formatDateTime(timePicker.date, "yyyy-MM-dd");
                        }
                    }
                }
            }

            Label{
                id: targetMonthLabel
                anchors.verticalCenter: targetMonthSelectorButton.verticalCenter
                text: i18n.tr("Month:")
                color: fontColor
            }

            /* open the popOver component with DatePicker */
            Button {
                id: targetMonthSelectorButton
                width: units.gu(20)
                color: buttonColor
                text: Qt.formatDateTime(new Date(), "MMMM yyyy")
                onClicked: {
                    PopupUtils.open(popoverTargetMonthPicker, targetMonthSelectorButton)
                }
            }

            Button {
                id: showChartButton
                width: units.gu(14)
                color: buttonColor
                text: i18n.tr("Update Chart")
                onClicked: {
                    drawGraph(settings.dataType);
                }
            }
        }

        //---------------- Chart ---------------------
        Grid {
            id: chartGridContainer
            visible: true
            columns:2                     // this property doesn't seem to be effective on the chart
            columnSpacing: units.gu(1)    // this property doesn't seem to be effective on the chart
            width: parent.width;
            height: parent.height;

            Rectangle {
                id: mBytesChartContainer
                visible: true    //Ubuntu.Components.Themes.Ambiance/SuruDark/SuruGradient
                color: bkgColor
                width: parent.width;
                height: parent.height - units.gu(15);

                property string moveYsteps;
                property double maxValue: maxScaleGraph;
                property double maxValueStored: maxScaleGraphStored;

                /* The monthly MBytes chart */
                QChart{
                    id: mBytesChart;
                    width: parent.width;
                    height: parent.height;
                    chartAnimated: false;
                    /* for all the options see: QChart.js */
                    chartOptions: {"barStrokeWidth": 0,
                                   "scaleFontColor": fontColor,
                                   "scaleLineColor": lineColor,
                                   "scaleGridLineColor": gridColor,
                                   "scaleMaxAxisYvalue": mBytesChartContainer.maxValue,
                                   "scaleMaxAxisYStored": mBytesChartContainer.maxValueStored,
                                    "XLabel": chartPage.graphXLabel
                                  };
                    /* chartData: set when the user press 'Show Chart' button */
                    chartType: Charts.ChartType.BAR;
                    onFinishedPaint: {
                      if (settings.dataType == "SIM") {
                          maxScaleGraph = settings.maxScaleYSIM;
                      }
                      rotateObj()
                    }
                }
                ChartInputArea {
                  id: inputArea
                  anchors.fill: parent
                  onTouchPressAndHold: alternateAction(x, y);
                  onAlternateAction: {
                      var dataXYreal = maxScaleGraph*(settings.axisY-settings.errorY1-y)/(settings.axisY-settings.errorY1-settings.errorY2)
                      var dataXY = Math.round(dataXYreal)
                      if (dataXY>0 & dataXY<maxScaleGraph) {
                              if (dataModel.count>0) {
                                    var i = dataModel.count-1;
                                    var found = false;
                                    while (i>=0 & found == false) {
                                       if (i>0) {
                                          if (dataXY >= dataModel.get(i).bytes) {
                                            if (dataXY == dataModel.get(i).bytes) {
                                              found = true;
                                            }
                                            i = i - 1;
                                          } else {
                                            if (dataXY < dataModel.get(i).bytes & dataXY != dataModel.get(i-1).bytes) {
                                              dataModel.insert(i+1, {"bytes": dataXY});
                                              createObj(y, dataXY);
                                              found = true;
                                              var info = StorageThresholds.insertThresholdValue(dataXY, settings.dataType)
                                              StorageThresholds.updateMutedValue(0, dataXY, settings.dataType)
                                            }
                                          }
                                       } else {
                                          if (dataXY > dataModel.get(i).bytes) {
                                            dataModel.insert(i, {"bytes": dataXY});
                                            createObj(y, dataXY)
                                          } else {
                                            if (dataXY < dataModel.get(i).bytes) {
                                                dataModel.insert(i+1, {"bytes": dataXY});
                                                createObj(y, dataXY)
                                            }
                                          }
                                          found = true;
                                          var info = StorageThresholds.insertThresholdValue(dataXY, settings.dataType)
                                          StorageThresholds.updateMutedValue(0, dataXY, settings.dataType)
                                       }
                                    }
                              } else {
                                StorageThresholds.createTables(settings.dataType)
                                dataModel.append({"bytes": dataXY});
                                createObj(y, dataXY)
                                var info = StorageThresholds.insertThresholdValue(dataXY, settings.dataType)
                                StorageThresholds.updateMutedValue(0, dataXY, settings.dataType)
                              }
                        }
                  }
                  Text {
                          id: dataExistenceText
                          anchors.top: parent.top
                          anchors.topMargin: parent.height/3
                          anchors.left: parent.left
                          anchors.leftMargin: parent.width/2 - units.gu(10)
                          text: i18n.tr("Data not available");
                          color: fontColor
                          visible: mBytesChartContainer.maxValue>0 ? false : true
                          font.pointSize: 40;
                  }
                  onTwoFingerSwipeYDetected: {
                      mBytesChartContainer.maxValue = maxScaleGraph + maxScaleGraph * steps / 15
                      if (settings.dataType == "WIFI") {
                         settings.maxScaleY = mBytesChartContainer.maxValue;
                         settings.maxScaleYStored = settings.maxScaleY;
                         maxScaleGraphStored = settings.maxScaleY;
                      } else {
                         settings.maxScaleYSIM = mBytesChartContainer.maxValue;
                         settings.maxScaleYSIMStored = settings.maxScaleYSIM;
                         maxScaleGraphStored = settings.maxScaleYSIM;
                      }
                      drawGraph(settings.dataType);
                  }
                }
            }
        }
    }

    BottomEdgeHint {
        id: bottomEdgeHint
        text: i18n.tr("Data thresholds list")
        onClicked: revealBottomEdge()
    }

    BottomEdge {
        id: bottomEdge
        height: parent.height
        contentComponent: Rectangle {
            width: bottomEdge.width
            height: bottomEdge.height
            color: bkgColor
            PageHeader {
              title: i18n.tr("Data thresholds list")

              StyleHints	{
                foregroundColor: fontColor
                backgroundColor: bkgColor
                dividerColor:	UbuntuColors.slate
              }
            }
            Rectangle {
                id: rectAnchor
                color: "transparent"
                width: parent.width
                height: units.gu(6)
            }
            Label{
                id: noThreshAvaiLabel
                anchors.top: rectAnchor.bottom
                anchors.centerIn: parent
                text: i18n.tr("No data thresholds available")
                textSize: Label.Large
                color: colorLabelThresh
            }
    		    ScrollView {
    			       anchors.top: rectAnchor.bottom
                 anchors.bottom: parent.bottom
                 anchors.right: parent.right
                 anchors.left: parent.left
    			       UbuntuListView {
                      id: listOfThresholds
              				anchors.fill: parent
              				model: dataModel
              				delegate:  ListItem {
                                    id: itemOfList
                                    contentItem.anchors {
                                        leftMargin: units.gu(5)
                                    }
    		                            height: units.gu(5)
                                    leadingActions: leading
    				                        trailingActions: trailing
    				                        ListItemLayout {
                          					    id: listItemLayout
                          					    title.text: modelData
                                        title.color: fontColor
                                        Label {
                                          color: fontColor
                                          text: index+1 + "."
                                          SlotsLayout.position: SlotsLayout.Leading;
                                        }
                                        Icon {
                                          id: audioIcon
                                          name: StorageThresholds.getMuteFlagFromThreshold(dataModel.get(index).bytes, settings.dataType) ? "audio-speakers-muted-symbolic" : "audio-speakers-symbolic"
                                          color: StorageThresholds.getMuteFlagFromThreshold(dataModel.get(index).bytes, settings.dataType) ? "red" : "green"
                                          SlotsLayout.position: SlotsLayout.Trailing;
                                          width: units.gu(2)
                                        }
                                        Icon {
                                          id: okIcon
                                          name: StorageThresholds.getNotifFlagFromThreshold(dataModel.get(index).bytes, settings.dataType) ? "ok" : ""
                                          SlotsLayout.position: SlotsLayout.Trailing-1;
                                          width: units.gu(2)
                                        }
                        				    }
                                    TextMetrics {
                                      id: textMetrics
                                      text: dataModel.get(index).bytes
                                    }
                                    TextMetrics {
                                      id: textMetricsPos
                                      text: index+1 + "."
                                    }
                                    SlotsLayout {
                                      id: layout
                                      mainSlot: Label {
                                          text: typeData
                                          color: fontColor
                                          SlotsLayout.padding.leading: textMetricsPos.advanceWidth + textMetrics.advanceWidth + units.gu(4)
                                      }
                                    }
                                    onContentMovementStarted: {
                                        listOfThresholds.currentIndex = index
                                    }
                                    MouseArea {
                                        anchors.fill: parent
                                        onClicked: listOfThresholds.currentIndex = index
                                    }
                                    ListItemActions {
                                        id: leading
                                        actions: Action {
                                            iconName: "delete"
                                            onTriggered: {
                                                  var found = false
                                                  var EOL = false
                                                  var i=0
                                                  var itemsNum = numberObj()
                                                  var index = listOfThresholds.currentIndex
                                                  while (i <= itemsNum-1 & found == false & EOL == false) {
                                                      var dataModelBytes = dataModel.get(listOfThresholds.currentIndex).bytes
                                                      var found = findAndDeleteObj(i, dataModelBytes);
                                                      if (i==itemsNum-1) {
                                                        EOL = true
                                                      }
                                                      i = i + 1
                                                  }
                                                  dataModel.remove(listOfThresholds.currentIndex, 1)
                                                  StorageThresholds.deleteThreshold(dataModelBytes, settings.dataType)
                                            }
                                        }
                                    }
                                    ListItemActions {
                                        id: trailing
                                        actions: [
                                            Action {
                                              iconName: "notification"
                                              parameterType: Action.Bool
                                              onTriggered: {
                                                    if (audioIcon.name == "audio-speakers-symbolic") {
                                                      StorageThresholds.updateMutedValue(1, dataModel.get(index).bytes, settings.dataType)
                                                      audioIcon.name = "audio-speakers-muted-symbolic"
                                                      audioIcon.color = "red"
                                                    } else {
                                                        if (audioIcon.name == "audio-speakers-muted-symbolic") {
                                                            StorageThresholds.updateMutedValue(0, dataModel.get(index).bytes, settings.dataType)
                                                            audioIcon.name = "audio-speakers-symbolic"
                                                            audioIcon.color = "green"
                                                        }
                                                    }
                                              }
                                            }
                                        ]
                                    }
    				                    }
                      currentIndex : -1
                      move: Transition {
                          id: moveTrans
                              NumberAnimation { properties: "x,y"; duration: 800; easing.type: Easing.OutBack }
                      }
                      displaced: Transition {
                          NumberAnimation { properties: "x,y"; duration: 400; easing.type: Easing.OutBounce }
                      }
    			       }
              }
        }
        onCommitStarted: {
          if (dataModel.count==0) {
                colorLabelThresh=fontColor;
              } else {
                colorLabelThresh = "transparent";
              }
        }
}


    function drawGraph(connType) {
             /* extract the year, month, day from the variable 'targetDate' that contains a value like yyyy-mm-dd */
             var dateParts = chartPage.targetDate.split("-");

             /* build a JS Date Object using string tokens (month is 0-based) */
             var date = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);

             /* calculates first and last day of the month */
             var firstDayMonth = new Date( date.getFullYear(),date.getMonth(), 1);
             var lastDayMonth = new Date( date.getFullYear(), date.getMonth() + 1, 0);

             /* set the data-set at the chart and make visible the chart and legend */
             mBytesChart.chartData = ChartUtils.getChartData(firstDayMonth,lastDayMonth,connType,lineColor,barColor);

             mBytesChartContainer.visible = true;
    }

    function createObj(y, data) {
        var i = chartPage.objects.length
        var component = Qt.createComponent("DataPlaceHolder.qml");
        chartPage.objects[i] = component.createObject(inputArea);
        chartPage.objects[i].x = settings.errorX+summitArrowOffset;
        chartPage.objects[i].y = y-25;
        chartPage.objects[i].bytesInfo = data;
        chartPage.objects[i].colorEdge = lineColor;
    }

    function findAndDeleteObj(i, confData) {
        var match = false
        if (chartPage.objects[i].bytesInfo == confData) {
          chartPage.objects[i].destroy()
          match = true
        }
        return match
    }

    function numberObj() {
      var items = chartPage.objects.length
      return items
    }

    function rotateObj() {
        var yAxis = new Array(chartPage.objects.length)
        if (maxScaleGraph>0) {
            if (chartPage.objects.length>0) {
                for (var i=0; i < chartPage.objects.length; i++) {
                      chartPage.objects[i].y = (maxScaleGraph*(settings.axisY-settings.errorY1) - chartPage.objects[i].bytesInfo*(settings.axisY-settings.errorY1-settings.errorY2))/maxScaleGraph-25
                      chartPage.objects[i].x = settings.errorX+summitArrowOffset;
                      if (chartPage.objects[i].bytesInfo > maxScaleGraph & chartPage.objects[i].colorEdge != "transparent") {
                        chartPage.objects[i].colorEdge = "transparent"
                      } else {
                        if (chartPage.objects[i].bytesInfo < maxScaleGraph & chartPage.objects[i].colorEdge == "transparent") {
                                chartPage.objects[i].colorEdge = lineColor
                        }
                      }
                  }
            }
        } else {
            if (chartPage.objects.length>0) {
                for (var i=0; i < chartPage.objects.length; i++) {
                    chartPage.objects[i].colorEdge = "transparent";
                }
            }
        }
    }

    function initOrdDataModel(value) {
        if (dataModel.count>0) {
              var i = dataModel.count-1;
              var found = false;
              while (i>=0 & found == false) {
                 if (i>0) {
                    if (value >= dataModel.get(i).bytes) {
                      if (value == dataModel.get(i).bytes) {
                        found = true;
                      }
                      i = i - 1;
                    } else {
                      if (value < dataModel.get(i).bytes & value != dataModel.get(i-1).bytes) {
                          dataModel.insert(i+1, {"bytes": value});
                        found = true;
                      }
                    }
                 } else {
                    if (value > dataModel.get(i).bytes) {
                        dataModel.insert(i, {"bytes": value});
                    } else {
                      if (value < dataModel.get(i).bytes) {
                          dataModel.insert(i+1, {"bytes": value});
                      }
                    }
                    found = true;
                 }
              }
        } else {
            dataModel.append({"bytes": value});
        }
    }

}
