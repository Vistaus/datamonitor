import QtQuick 2.4

Rectangle {
    id: rect
    width: 100; height: 50
//    width: 200
//    height: 2
//    color: "black"
    color: "transparent"
//    border.color: "white"
    border.color: "transparent"

//    property string bytesInfo: "NaN"
    property double bytesInfo: 0
    property double summitArrowOffset: 14.43375673
    property double diagonalLength: 28.86751346
    property double verticalOffset: -14
    property double horizontalOffset: -8.5
//    property string colorEdge: "white"
    property string colorEdge

//    NumberAnimation on opacity {
//        to: 0
//        duration: 1000

//        onRunningChanged: {
//            if (!running) {
//                console.log("Destroying...")
//                rect.destroy();
//            }
//        }
//    }

    Rectangle {
      id: lineTop
      anchors.top: rect.top
      anchors.left: rect.left
      anchors.right: rect.right
      height: 2
//      color: "white"
      color: colorEdge
    }
    Rectangle {
      id: lineBottom
      anchors.top: rect.bottom
      anchors.left: rect.left
      anchors.right: rect.right
      height: 2
//      color: "white"
      color: colorEdge
    }
    Rectangle {
      id: lineRightTop
      anchors.bottom: rect.top
      anchors.bottomMargin: verticalOffset
      anchors.left: rect.right
      anchors.leftMargin: horizontalOffset
      width: diagonalLength
//      transformOrigin: lineRightTop.TopLeft
      rotation: 60
      height: 2
//      color: "white"
      color: colorEdge
    }
    Rectangle {
      id: lineLeftTop
      anchors.bottom: rect.top
      anchors.bottomMargin: verticalOffset
      anchors.right: rect.left
      anchors.rightMargin: horizontalOffset
      width: diagonalLength
//      transformOrigin: lineLeftTop.TopRight
      rotation: -60
      height: 2
//      color: "white"
      color: colorEdge
    }
    Rectangle {
      id: lineRightBottom
      anchors.top: rect.bottom
      anchors.topMargin: verticalOffset
      anchors.left: rect.right
      anchors.leftMargin: horizontalOffset
      width: diagonalLength
//      transformOrigin: lineRightBottom.TopLeft
      rotation: -60
      height: 2
//      color: "white"
      color: colorEdge
    }
    Rectangle {
      id: lineLeftBottom
      anchors.top: rect.bottom
      anchors.topMargin: verticalOffset
      anchors.right: rect.left
      anchors.rightMargin: horizontalOffset
      width: diagonalLength
//      transformOrigin: lineLeftBottom.TopRight
      rotation: 60
      height: 2
//      color: "white"
      color: colorEdge
    }
    Rectangle {
      id: line
      anchors.top: rect.top
      anchors.topMargin: rect.height/2
      anchors.left: rect.right
      anchors.leftMargin: summitArrowOffset
      width: 5000
      height: 2
//      color: "white"
      color: colorEdge
    }
    Text {
      id: textField
      anchors.centerIn: parent
      fontSizeMode: Text.HorizontalFit
//      color: "white"
      color: colorEdge
      text: bytesInfo
    }
}
